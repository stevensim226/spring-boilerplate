package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControllerMain {

    @GetMapping("/")
    public String mainPage(Model model) {
        model.addAttribute("greeting", "こんにちは");
        return "index/index";
    }
}
